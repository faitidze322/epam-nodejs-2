const mongoose = require('mongoose');

// Название докумета в БД зависит от первого
// аргумунта переданого в конструктор model
const BannedToken = mongoose.model('BannedToken', {
  value: {
    type: String,
    required: true,
  },
  expirationDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {BannedToken};
