const mongoose = require('mongoose');

// Название докумета в БД зависит от первого
// аргумунта переданого в конструктор model
const User = mongoose.model('User', {
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  email: String,
  createdAt: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {User};
