const mongoose = require('mongoose');

// Название докумета в БД зависит от первого
// аргумунта переданого в конструктор model
const Note = mongoose.model('Note', {
  text: {
    type: String,
    required: true,
  },
  checked: {
    type: Boolean,
    default: false,
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
});

module.exports = {Note};
