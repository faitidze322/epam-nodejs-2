/**
* Создает экземпляр NodeCourseError.
*
* @constructor
* @this  {NodeCourseError}
* @param {string} message - сообщение ошибки
* @property {number} status - статус ошибки
*/
class NodeCourseError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

/**
* Создает экземпляр InvalidRequestError.
*
* @constructor
* @this  {InvalidRequestError}
* @param {string} message - сообщение ошибки
* @property {number} status - статус ошибки
*/
class InvalidRequestError extends NodeCourseError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  NodeCourseError,
  InvalidRequestError,
};
