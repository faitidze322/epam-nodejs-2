require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();

const PORT = process.env.PORT;

const {authRouter} = require('./controllers/authController');
const {notesRouter} = require('./controllers/notesController');
const {usersRouter} = require('./controllers/usersController');
const {authMiddleware} = require('./middlewares/authMiddleware');

// Errors
const {NodeCourseError} = require('./util/errors');

app.use(express.static('./dist'));
app.use(express.json());
app.use(morgan('tiny'));

// Public API routes
app.use('/api/auth', authRouter);

// Protected API routes
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users', [authMiddleware], usersRouter);

app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});


// Start
(async () => {
  try {
    await mongoose.connect(
        process.env.DB,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useFindAndModify: false,
        },
    );
    app.listen(PORT, () => {
      console.log(`Server started on port ${PORT}`);
    });
  } catch (err) {
    console.error(`Startup server error: ${err}`);
  }
})();
