const express = require('express');
const router = express.Router();

const {
  getUserDataById,
  deleteUserById,
  changePasswordByUserId,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../util/apiUtils');

router.get('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserDataById(userId);

  res.status(200).json(user);
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {
    authorization,
  } = req.headers;
  const [, token] = authorization.split(' ');


  await deleteUserById(userId, token);

  res.status(200).json({message: 'Your account deleted successfully!'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {password} = req.body;

  await changePasswordByUserId(userId, password);

  res.status(200).json({message: 'Your password changed successfully!'});
}));

module.exports = {
  usersRouter: router,
};
