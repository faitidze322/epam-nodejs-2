const express = require('express');
const router = express.Router();

const {
  getNotesByUserId,
  getUsersNoteById,
  addNoteToUser,
  updateUsersNoteById,
  toggleNoteCheckById,
  deleteUsersNoteById,
} = require('../services/notesService');

// Util
const {
  asyncWrapper,
} = require('../util/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {
    offset=0, limit=0,
  } = req.query;

  const notes = await getNotesByUserId(
      userId,
      parseInt(offset),
      parseInt(limit),
  );

  res.status(200).json({...notes});
}));

router.get('/:noteId', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {noteId} = req.params;

  const note = await getUsersNoteById(noteId, userId);

  res.status(200).json({note});
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await addNoteToUser(userId, req.body);

  res.json({message: 'Note successfully added!'});
}));

router.put('/:noteId', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {noteId} = req.params;

  const data = req.body;

  await updateUsersNoteById(noteId, userId, data);

  res.json({message: 'Note updated successfully'});
}));

router.patch('/:noteId', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {noteId} = req.params;

  await toggleNoteCheckById(noteId, userId);

  res.json({message: 'Check successfully toggled!'});
}));

router.delete('/:noteId', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {noteId} = req.params;

  await deleteUsersNoteById(noteId, userId);

  res.json({message: 'Note successfully deleted!'});
}));

module.exports = {
  notesRouter: router,
};
