const express = require('express');
const router = express.Router();

const {registration, signIn} = require('../services/authService');

// Util
const {
  asyncWrapper,
} = require('../util/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;

    await registration({username, password});

    res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
}));

router.post('/login', asyncWrapper(async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;

    const jwt_token = await signIn({username, password});

    res.status(200).json({
      jwt_token,
      message: 'Logged in successfully!',
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
}));

module.exports = {
  authRouter: router,
};
