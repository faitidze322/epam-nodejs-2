const jwt = require('jsonwebtoken');

const authMiddleware = async (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({
      message: 'Please, provide "authorization" header',
    });
  }

  // Достаём второй елемент из массива
  // token типа Bearer
  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    return res.status(401).json({
      message: 'Please, include token to header',
    });
  }

  try {
    const tokenPayload = jwt.verify(jwt_token, process.env.JWT);

    req.user = {
      userId: tokenPayload._id,
      username: tokenPayload.username,
    };
    next();
  } catch (err) {
    res.status(401).json({message: err.message});
  }
};

module.exports = {
  authMiddleware,
};
