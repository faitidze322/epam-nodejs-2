const {User} = require('../models/userModel');
const {BannedToken} = require('../models/bannedTokenModel');
const bcrypt = require('bcrypt');

const getUserDataById = async (userId) => {
  const user = await User.findOne(
      {_id: userId}).select(['-password', '-__v'],
  );
  return user;
};

const deleteUserById = async (userId, token) => {
  await User.findOneAndRemove({_id: userId});
  const bannedToken = new BannedToken({
    value: token, expirationDate: Date.now(),
  });

  await bannedToken.save();
};

const changePasswordByUserId = async (userId, newPassword) => {
  const hashedPass = await bcrypt.hash(newPassword, 10);

  await User.findOneAndUpdate(
      {_id: userId},
      {password: await hashedPass},
  );
};

module.exports = {
  getUserDataById,
  deleteUserById,
  changePasswordByUserId,
};
