const {Note} = require('../models/noteModel');
const {CastError} = require('mongoose').Error;

// Errors
const {
  InvalidRequestError,
} = require('../util/errors');

const getNotesByUserId = async (userId, offset, notesLimit) => {
  const notes = await Note.find({userId}).skip(offset).limit(notesLimit);
  const notesAmount = await Note.countDocuments({userId});

  return {
    offset: offset,
    limit: notesLimit,
    count: notesAmount,
    notes,
  };
};

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});

  await note.save();
};

const getUsersNoteById = async (noteId, userId) => {
  try {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
  } catch (err) {
    if (err instanceof CastError) {
      throw new InvalidRequestError('Note not found!');
    }
  }
};

const updateUsersNoteById = async (noteId, userId, newData) => {
  try {
    await Note.findOneAndUpdate({_id: noteId, userId}, {$set: newData});
  } catch (err) {
    if (err instanceof CastError) {
      throw new InvalidRequestError('Note not found!');
    }
  }
};

const deleteUsersNoteById = async (noteId, userId) => {
  try {
    await Note.findOneAndRemove({_id: noteId, userId});
  } catch (err) {
    if (err instanceof CastError) {
      throw new InvalidRequestError('Note not found!');
    }
  }
};

const toggleNoteCheckById = async (noteId, userId) => {
  const note = await getUsersNoteById(noteId, userId);
  const {checked: currentStatus} = note;

  await updateUsersNoteById(noteId, userId, {checked: !currentStatus});
};

module.exports = {
  getNotesByUserId,
  addNoteToUser,
  getUsersNoteById,
  updateUsersNoteById,
  toggleNoteCheckById,
  deleteUsersNoteById,
};
